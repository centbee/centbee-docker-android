# Centbee Android image

### Pull newest build from Docker Hub
```
docker pull centbee/android:latest
```

### Run image
```
docker run -it centbee/android:latest bash
```

### Use as base image
```Dockerfile
FROM centbee/android:latest
```
